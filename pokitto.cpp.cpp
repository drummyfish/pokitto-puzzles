/**
  Pokitto frontend.

  author: Miloslav Ciz
  license: MIT
*/

#include "Pokitto.h"

#define FPS 60

#define INFO_BAR_HEIGHT 8

#define DISPLAY_WIDTH 110
#define DISPLAY_HEIGHT 88

#define COLOR_TRANSPARENT 16
#define COLOR_DEFAULT 15
#define COLOR_WHITE 12
#define COLOR_BLACK 10
#define COLOR_GRAY 11

#define MAX_GAME_COLORS 10

#define clamp(val,v0,v1) ((val) > (v1) ? (v1) : ((val) < (v0) ? (v0) : (val)))

static float dt = 1.0 / FPS;

extern "C"
{
#include "puzzles.h"
}

Pokitto::Core pokitto;

int puzzleW = DISPLAY_WIDTH, puzzleH = DISPLAY_HEIGHT - INFO_BAR_HEIGHT - 1;

bool timerOn = false;

bool clipOn = false;

int clipX1 = 0, clipX2 = DISPLAY_WIDTH, clipY1 = 0, clipY2 = DISPLAY_HEIGHT;

unsigned short palette[16];

#if 0
  #include "stdio.h"
  #define debug(s) printf("%s\n",s);
  #define debugI(num) printf("%d\n",num);
#else
  #define debug(s) ;
  #define debugI(num) ;
#endif

void deactivate_timer(frontend *fe)
{
  debug("deact timer");

  timerOn = false;
}

void debug_printf(const char *fmt, ...)
{
  //printf("DEBUG: %s\n",fmt);
}

void activate_timer(frontend *fe)
{
  debug("act timer");

  timerOn = true;
}

void fatal(const char *fmt, ...)
{
  debug("fatal");
  //printf(fmt);
}

void frontend_default_colour(frontend *fe, float *output)
{
  debug("fe default color");

  output[0] = 0.5;
  output[1] = 0.5;
  output[2] = 0.5;
}

void get_random_seed(void **randseed, int *randseedsize)
{
  debug("get random seed");

  *randseed = malloc(4);
  **((uint32_t **) randseed) = pokitto.getTime();
  *randseedsize = 4;
}

static void pok_status_bar(void *handle, const char *text)
{
  debug("status bar");

  pokitto.display.setColor(COLOR_BLACK);
  pokitto.display.fillRectangle(0,88 - INFO_BAR_HEIGHT,DISPLAY_WIDTH,INFO_BAR_HEIGHT);
  pokitto.display.setCursor(1,88 - INFO_BAR_HEIGHT + 1);
  pokitto.display.setColor(COLOR_WHITE);
  pokitto.display.bgcolor = COLOR_TRANSPARENT;
  pokitto.display.print(text);
}

static blitter *pok_blitter_new(void *handle, int w, int h)
{
  debug("blitter new");
}

static void pok_blitter_free(void *handle, blitter *bl)
{
  debug("blitter free");
}

static void blitter_mkbitmap(frontend *fe, blitter *bl)
{
  debug("blitter mkbitmap");
}

static void pok_blitter_save(void *handle, blitter *bl, int x, int y)
{
  debug("blitter save");
}

static void pok_blitter_load(void *handle, blitter *bl, int x, int y)
{
  debug("blitter load");
}

static void pok_set_brush(frontend *fe, int colour)
{
  debug("blitter set brush");
}

static void pok_reset_brush(frontend *fe)
{
  debug("blitter reset brush");
}

static void pok_set_pen(frontend *fe, int colour, int thin)
{
  debug("blitter set pen");
}

static void pok_reset_pen(frontend *fe)
{
  debug("blitter reset brush");
}

static void pok_clip(void *handle, int x, int y, int w, int h)
{
  debug("blitter clip");

  clipX1 = x;
  clipY1 = y;
  clipX2 = x + w;
  clipY2 = y + h;

  clipOn = true;
}

static void pok_unclip(void *handle)
{
  debug("blitter unclip");

  clipOn = false;
}

static void pok_draw_text(void *handle, int x, int y, int fonttype,
              int fontsize, int align, int colour, const char *text)
{
  if (clipOn)
  {
    int textLen = 0;

    while (text[textLen] != 0 && textLen < 128)
      textLen++;

    if (x < clipX1 || y < clipY1 ||
      x + textLen * 3 > clipX2 ||
      y + 5 > clipY2)
      return;
  }

  pokitto.display.setColor(colour);

  pokitto.display.bgcolor = COLOR_TRANSPARENT;

  pokitto.display.setCursor(x,y);
  pokitto.display.print(text);

  debug("draw text");
}

static void pok_draw_rect(void *handle, int x, int y, int w, int h, int colour)
{
  debug("draw rect");
  debugI(x);
  debugI(y);
  debugI(w);
  debugI(h);
  debugI(colour);

  if (clipOn)
  {
    int x2 = x + w, y2 = y + h;

    x = max(x,clipX1);
    y = max(y,clipY1);

    x2 = min(x2,clipX2);
    y2 = min(y2,clipY2);

    w = x2 - x;
    h = y2 - y;
  }

  pokitto.display.setColor(colour % MAX_GAME_COLORS);
  pokitto.display.fillRectangle(x,y,w,h);

  pokitto.display.setColor(COLOR_GRAY);
  pokitto.display.drawRectangle(x,y,w,h);
}

static void pok_draw_line(void *handle, int x1, int y1, int x2, int y2, int colour)
{
  debug("draw line");

  if (clipOn)
  {
    //TODO FIXME

    x1 = clamp(x1,clipX1,clipX2);
    y1 = clamp(y1,clipY1,clipY2);
  }

  pokitto.display.setColor(colour);
  pokitto.display.drawLine(x1,y1,x2,y2);
}

static void pok_draw_circle(void *handle, int cx, int cy, int radius,
                int fillcolour, int outlinecolour)
{
  debug("draw circle");
  debugI(cx);
  debugI(cy);
  debugI(radius);

  radius = radius == 0 ? 1 : radius;

  if (fillcolour >= 0)
  {
    pokitto.display.setColor(fillcolour % MAX_GAME_COLORS);
    pokitto.display.fillCircle(cx,cy,radius);
  }

  pokitto.display.setColor(outlinecolour % MAX_GAME_COLORS);
  pokitto.display.drawCircle(cx,cy,radius);
}

static void pok_draw_polygon(void *handle, int *coords, int npoints,
                 int fillcolour, int outlinecolour)
{
  debug("draw polygon");

  debugI(npoints);

  if (clipOn)
  {
    for (int i = 0; i < npoints * 2; i += 2)
    {
      coords[i] = clamp(coords[i],clipX1,clipX2);
      coords[i + 1] = clamp(coords[i + 1],clipY1,clipY2);
    }
  }

  if (fillcolour >= 0)
  {
    pokitto.display.setColor(fillcolour);

    int32_t cx = 0, cy = 0;

    for (int i = 0; i < (2 * npoints); i += 2)
    {
      cx += coords[i];
      cy += coords[i + 1];
    }

    cx /= npoints;
    cy /= npoints;

    for (int i = 0; i < (2 * npoints - 2); i += 2)
      pokitto.display.fillTriangle(cx,cy,coords[i],
        coords[i + 1],coords[i + 2],coords[i + 3]);
    
    pokitto.display.fillTriangle(cx,cy,coords[0],
      coords[1],coords[2 * npoints - 2],coords[2 * npoints - 1]);
  }

  pokitto.display.setColor(outlinecolour);

  for (int i = 0; i < (2 * npoints - 2); i += 2)
    pokitto.display.drawLine(coords[i],coords[i + 1],
                             coords[i + 2],coords[i + 3]);

  pokitto.display.drawLine(coords[2 * npoints - 2],coords[2 * npoints - 1],
                             coords[0],coords[1]);
}

static void pok_start_draw(void *handle)
{
  debug("start draw");
}

static void pok_draw_update(void *handle, int x, int y, int w, int h)
{
  debug("draw update");

  debugI(x);
  debugI(y);
  debugI(w);
  debugI(h);
}

static void pok_end_draw(void *handle)
{
  debug("end draw");
}

static void pok_line_width(void *handle, float width)
{
  debug("line width");
}

static void pok_line_dotted(void *handle, int dotted)
{
  debug("line dotted");
}

static void pok_begin_doc(void *handle, int pages)
{
  debug("begin doc");
}

static void pok_begin_page(void *handle, int number)
{
  debug("begin page");
}

static void pok_begin_puzzle(void *handle, float xm, float xc,
                 float ym, float yc, int pw, int ph, float wmm)
{
  debug("begin puzzle");
}

static void pok_end_puzzle(void *handle)
{
  debug("end puzzle");
}

static void pok_end_page(void *handle, int number)
{
  debug("end page");
}

static void pok_end_doc(void *handle)
{
  debug("end doc");
}

char *pok_text_fallback(void *handle, const char *const *strings, int nstrings)
{
  debug("text fallback");

  int len = 0;

  while (strings[0][len] != 0 && len < 128)
    len++;

  char *result = (char *) malloc(len + 1);

  for (int i = 0; i < len; ++i)
    result[i] = strings[0][i];

  result[len] = 0;

  return result;
}

struct frontend
{
  midend *me;
  int timer_active;
  struct timeval last_time;
  config_item *cfg;
  int cfg_which, cfgret;
  int ox, oy, w, h;
};

const struct drawing_api pokitto_drawing =
{
  pok_draw_text,
  pok_draw_rect,
  pok_draw_line,
  pok_draw_polygon,
  pok_draw_circle,
  NULL, // update
  pok_clip,
  pok_unclip,
  pok_start_draw,
  pok_end_draw,
  pok_status_bar,
  pok_blitter_new,
  pok_blitter_free,
  pok_blitter_save,
  pok_blitter_load,
  pok_begin_doc,
  pok_begin_page,
  pok_begin_puzzle,
  pok_end_puzzle,
  pok_end_page,
  pok_end_doc,
  pok_line_width,
  pok_line_dotted,
  pok_text_fallback,
  NULL // line thick
};

void clearDisplay(int color=COLOR_DEFAULT)
{
  pokitto.display.setColor(color);
  pokitto.display.fillRectangle(0,0,DISPLAY_WIDTH,DISPLAY_HEIGHT);
}

#define MENU_NUMITEMS 3

char *menuItems[] =
{
  "new",
  "restart",
  "solve"
};

enum GameState
{
  STATE_PUZZLE_CHOOSE,
  STATE_GAME_PLAYING,
  STATE_MENU
};

//-----------------------------------------
// global vars:

int menuItem = 0;
int gameState = STATE_PUZZLE_CHOOSE;
struct frontend fe;
float *colors;
int numColors;

//-----------------------------------------

void drawMenu()
{
  const int menuStrtX = 60;

  pokitto.display.setColor(COLOR_GRAY);
  pokitto.display.fillRectangle(menuStrtX,0,DISPLAY_WIDTH - menuStrtX,DISPLAY_HEIGHT);

  pokitto.display.bgcolor = COLOR_TRANSPARENT;
  pokitto.display.setColor(COLOR_BLACK);

  for (int i = 0; i < MENU_NUMITEMS; ++i)
  {
    if (i == menuItem)
    {
      pokitto.display.setColor(COLOR_WHITE);
      pokitto.display.fillRectangle(menuStrtX + 1, i * 7 + 1, DISPLAY_WIDTH, 7);
      pokitto.display.setColor(COLOR_BLACK);
    }

    pokitto.display.setCursor(menuStrtX + 1, i * 7 + 1);
    pokitto.display.print(menuItems[i]);
  }
}

void drawGameList()
{
  // note: game palette is not yet loaded here, default is in use

  clearDisplay(0);

  int y = 1;

  for (int i = 0; i < gamecount; ++i) 
  {
    if (menuItem == i)
    {
      pokitto.display.setColor(2);
      pokitto.display.fillRectangle(1,y,DISPLAY_WIDTH / 2,5);
    }

    pokitto.display.setCursor(1,y);
    pokitto.display.setColor(1);
    pokitto.display.bgcolor = COLOR_TRANSPARENT;
    pokitto.display.print(gamelist[i]->name);

    y += 6;
  }
}

void menuConfirmed(midend *me)
{
  switch (menuItem)
  {
    case 0:
      debug("new");
      midend_new_game(me);

      clearDisplay();
      midend_force_redraw(me);
      gameState = STATE_GAME_PLAYING;

      break;

    case 1:
      debug("restart");
      midend_restart_game(me);
      clearDisplay();
      midend_force_redraw(me);
      gameState = STATE_GAME_PLAYING;
      break;

    case 2:
    {
      debug("solve");
      const char *res = midend_solve(me);

      if (res != NULL)
      {
        debug("couldn't solve!");
      }
      else
      {
        clearDisplay();
        midend_force_redraw(me);
        gameState = STATE_GAME_PLAYING;
        break;
      }
    }

    default:
      break;
  }
}

void initGame(int index)
{
  if (!gamelist[index]->wants_statusbar)
    puzzleH = DISPLAY_HEIGHT; // if no statusbar, use the full display height

  fe.me = midend_new(&fe, gamelist[index], &pokitto_drawing, &fe);   
  colors = midend_colours(fe.me,&numColors);
  numColors = min(MAX_GAME_COLORS,numColors);

  debug("colors");

  float defaultColor[3];
  frontend_default_colour(NULL,defaultColor);

  palette[COLOR_BLACK]   = pokitto.display.RGBto565(0,0,0);
  palette[COLOR_GRAY]    = pokitto.display.RGBto565(64,64,64);
  palette[COLOR_WHITE]   = pokitto.display.RGBto565(255,255,255);
  palette[13] = 0;
  palette[14] = 0;
  palette[COLOR_DEFAULT] = pokitto.display.RGBto565(
                             defaultColor[0] * 255,
                             defaultColor[1] * 255,
                             defaultColor[2] * 255);

  for (int i = 0; i < 8; ++i)
  {
    if (i < numColors)
    {
      debugI(i);

      palette[i] =
          pokitto.display.RGBto565(
            colors[3 * i] * 255,
            colors[3 * i + 1] * 255,
            colors[3 * i + 2] * 255);
    }
    else
  
      palette[i] = pokitto.display.RGBto565(
        (70 * i) % 255,
        (85 * i) % 255,
        (100 * i) % 255);
  }

  pokitto.display.load565Palette(palette);

  clearDisplay();

  midend_new_game(fe.me);

  midend_reset_tilesize(fe.me);
  midend_size(fe.me,&puzzleW,&puzzleH,1);

  midend_which_preset(fe.me);
  midend_redraw(fe.me);
}

int main()
{
  pokitto.begin(); 
  pokitto.setFrameRate(FPS);
  pokitto.display.setFont(fontTiny);
  pokitto.display.persistence = 1;
  pokitto.display.setInvisibleColor(COLOR_TRANSPARENT);
  pokitto.display.setFont(font3x5);

  debug("start");

  while (pokitto.isRunning())
  {
    if (pokitto.update())
    {
      switch (gameState)
      {
        case STATE_GAME_PLAYING:
        {
          if (timerOn)
            midend_timer(fe.me,dt);

          #define checkButton(b,k)\
            if (pokitto.buttons.timeHeld(b) == 1)\
              midend_process_key(fe.me,0,0,k);

          checkButton(BTN_UP,CURSOR_UP)
          checkButton(BTN_DOWN,CURSOR_DOWN)
          checkButton(BTN_LEFT,CURSOR_LEFT)
          checkButton(BTN_RIGHT,CURSOR_RIGHT)
          checkButton(BTN_A,CURSOR_SELECT)
          checkButton(BTN_B,CURSOR_SELECT2)

          if (pokitto.buttons.timeHeld(BTN_C) == 1)
            gameState = STATE_MENU;

          #undef checkButton
  
          break;
        }

        case STATE_MENU:
          drawMenu();

          if (pokitto.buttons.timeHeld(BTN_UP) == 1)
            menuItem = max(0,menuItem - 1);
          else if (pokitto.buttons.timeHeld(BTN_DOWN) == 1)
            menuItem = min(MENU_NUMITEMS - 1,menuItem + 1);
          else if (pokitto.buttons.timeHeld(BTN_A) == 1)
            menuConfirmed(fe.me);
          else if (pokitto.buttons.timeHeld(BTN_C) == 1)
          {
            gameState = STATE_GAME_PLAYING;
            clearDisplay();
            midend_force_redraw(fe.me);
          }

          break;

        case STATE_PUZZLE_CHOOSE:
          drawGameList();

          if (pokitto.buttons.timeHeld(BTN_UP) == 1)
            menuItem = menuItem <= 0 ? gamecount - 1 : menuItem - 1;
          else if (pokitto.buttons.timeHeld(BTN_DOWN) == 1)
            menuItem = menuItem >= gamecount - 1 ? 0 : menuItem + 1;
          else if (pokitto.buttons.timeHeld(BTN_A) == 1)
          {
            initGame(menuItem);
            gameState = STATE_GAME_PLAYING;
          }

          break;

        default:
          break;
      }
    }
  }

  midend_free(fe.me);

  debug("end");
  return 0;
}
